package week7.day2;

import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import pages.LoginPage;
//import wdMethods_Original.ProjectMethods;
import wdMethods_Original.ProjectMethods2;

public class CreateLeadUsingPOM extends ProjectMethods2{
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "LeafTapsLearnings_Create_Leads_POM";
		testCaseDesc = "Create New Lead";
		category = "smoke";
		author ="Vetri";
		excel ="dataset2";
	}
	
	@Test(dataProvider="fetchdata")
	public void createLead(String uName, String passWord,String cName, String fName, String lName)
	{
		new LoginPage()
		.typeUserName(uName)
		.typePassword(passWord)
		.clickLoginButton()
		.clickCrm()
		.clickLead()
		.clickCreateLead()
		.typeCompanyName(cName)
		.typeFirstName(fName)
		.typeLastName(lName)
		.clickCreateLeadButton()
		.verifyFirstName(fName);
	}
	
}
