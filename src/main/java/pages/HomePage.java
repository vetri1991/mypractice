package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods_Original.ProjectMethods;
import wdMethods_Original.ProjectMethods2;

public class HomePage extends ProjectMethods2{

	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT,using ="CRM/SFA")
	WebElement eleClick;
	public MyHome clickCrm()
	{
		//WebElement clickLogin = locateElement("class", "decorativeSubmit");
		click(eleClick);
		return new MyHome();
		
	}
}
