package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods_Original.ProjectMethods;
import wdMethods_Original.ProjectMethods2;

public class MyLeads extends ProjectMethods2{

	public MyLeads()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.XPATH,using ="//a[text()='Create Lead']")
	WebElement eleClick;
	public CreateLead clickCreateLead()
	{
		//WebElement clickLogin = locateElement("class", "decorativeSubmit");
		click(eleClick);
		return new CreateLead();
		
	}
}
