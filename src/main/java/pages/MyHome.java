package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods_Original.ProjectMethods;
import wdMethods_Original.ProjectMethods2;

public class MyHome extends ProjectMethods2{

	public MyHome()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT,using ="Leads")
	WebElement eleClick;
	public MyLeads clickLead()
	{
		//WebElement clickLogin = locateElement("class", "decorativeSubmit");
		click(eleClick);
		return new MyLeads();
		
	}
}
