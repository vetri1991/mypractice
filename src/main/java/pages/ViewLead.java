package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods_Original.ProjectMethods;
import wdMethods_Original.ProjectMethods2;

public class ViewLead extends ProjectMethods2{

	public ViewLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(how = How.LINK_TEXT,using="Velavan")
	WebElement eleFname;
	public void verifyFirstName(String data)
	{
		verifyExactText(eleFname, data);
		
	}
}
