package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods_Original.ProjectMethods;
import wdMethods_Original.ProjectMethods2;

public class LoginPage extends ProjectMethods2{

	public LoginPage()
	{
		PageFactory.initElements(driver, this);
	}
	@FindBy(id="username")
	WebElement eleUsername;
	public LoginPage typeUserName(String data)
	{
		//WebElement userName = locateElement("id", "username");
		type(eleUsername, data);
		return this;
		
	}
	
	@FindBy(id="password")
	WebElement elePassword;
	public LoginPage typePassword(String data)
	{
		//WebElement userName = locateElement("id", "password");
		type(elePassword, data);
		return this;
		
	}
	
	/*public LoginPage verifyUserNameColor()
	{
		return null;
		
	}*/
	
	@FindBy(how = How.CLASS_NAME,using ="decorativeSubmit")
	WebElement eleClick;
	public HomePage clickLoginButton()
	{
		//WebElement clickLogin = locateElement("class", "decorativeSubmit");
		click(eleClick);
		return new HomePage();
	}
}
