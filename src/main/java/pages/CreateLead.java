package pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.How;
import org.openqa.selenium.support.PageFactory;

import wdMethods_Original.ProjectMethods;
import wdMethods_Original.ProjectMethods2;

public class CreateLead extends ProjectMethods2{

	public CreateLead()
	{
		PageFactory.initElements(driver, this);
	}
	
	@FindBy(id="createLeadForm_companyName")
	WebElement eleCompanyName;
	public CreateLead typeCompanyName(String data)
	{
		type(eleCompanyName, data);
		return this;
	}
	
	@FindBy(id="createLeadForm_firstName")
	WebElement elefirstName;
	public CreateLead typeFirstName(String data)
	{
		type(elefirstName, data);
		return this;
	}
	
	@FindBy(id="createLeadForm_lastName")
	WebElement elelastName;
	public CreateLead typeLastName(String data)
	{
		type(elelastName, data);
		return this;
	}
	
	
	@FindBy(how = How.CLASS_NAME,using ="smallSubmit")
	WebElement eleClick;
	public ViewLead clickCreateLeadButton()
	{
		//WebElement clickLogin = locateElement("class", "decorativeSubmit");
		click(eleClick);
		return new ViewLead();
		
	}
}
