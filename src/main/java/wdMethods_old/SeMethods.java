package wdMethods_old;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.Select;



public class SeMethods implements WdMethods{
	public RemoteWebDriver driver;
	public int i = 1;
	public void startApp(String browser, String url) {
		if(browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
			driver  = new ChromeDriver();
		} else if(browser.equalsIgnoreCase("firefox")) {
			System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
			driver  = new FirefoxDriver();
		}
		driver.get(url);
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		System.out.println("The Browser "+browser+" launched successfully");
		takeSnap();

	}


	public WebElement locateElement(String locator, String locValue) {
		switch (locator) {
		case "id": return driver.findElementById(locValue);
		case "class": return driver.findElementByClassName(locValue);
		case "xpath": return driver.findElementByXPath(locValue);
		case "linktext": return driver.findElementByLinkText(locValue);	
		}
		return null;
	}

	
	public WebElement locateElement(String locValue) {		
		return driver.findElement(By.id(locValue));
		
	}

	public void type(WebElement ele, String data) {
		ele.sendKeys(data);
		System.out.println("The Data "+data+" is entered Successfully");
		takeSnap();
	}


	public void click(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		takeSnap();
	}
	
	public void clickWithNoSnap(WebElement ele) {
		ele.click();
		System.out.println("The Element "+ele+" is clicked Successfully");
		
	}

	
	public String getText(WebElement ele) {
		
		String text = ele.getText();
		return text;
		
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		
		Select scr = new Select(ele);
		scr.selectByVisibleText(value);
		takeSnap();

	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		
		Select scr = new Select(ele);
		scr.selectByIndex(index);
		takeSnap();

	}

	@Override
	public boolean verifyTitle(WebElement ele,String expectedTitle) {
		boolean result=false;
		String text = getText(ele);
		if(expectedTitle.equals(text))
		{
			result =true;
		}
		return result;
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		if(expectedText.equals(getText(ele)))
		{
			
			System.out.println("Tect Matched");
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		String text = ele.getText();
		if(text.contains(expectedText))
		{
			System.out.println("Mentioned text "+expectedText+" is available");
		}
		else
		{
			System.out.println("Mentioned text "+expectedText+" is not available");
		}

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		
		//String attribute2 = ele.getAttribute(attribute);
		

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		
		if(ele.isSelected())
		{
			System.out.println("Elemented is selected");
		}

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		
		if(ele.isDisplayed())
		{
			System.out.println("Element is Visible");
		}

	}

	@Override
	public void switchToWindow(int index) {
		
		Set<String> windowHandles = driver.getWindowHandles();
		List<String> list = new ArrayList<String>();
		windowHandles.addAll(list);
		driver.switchTo().window(list.get(index));
		takeSnap();

	}

	@Override
	public void switchToFrame(WebElement ele) {
		
		driver.switchTo().frame(ele);
		takeSnap();

	}

	@Override
	public void acceptAlert() {
		
		driver.switchTo().alert().accept();

	}

	@Override
	public void dismissAlert() {
		
		driver.switchTo().alert().dismiss();

	}

	@Override
	public String getAlertText() {
		String text = driver.switchTo().alert().getText();
		return text;
	}


	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File desc = new File("./snaps/img"+i+".png");		
		FileUtils.copyFile(src, desc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		
		driver.quit();

	}


	@Override
	public void verifyContentMatch(String value, String match) {
		
		if(value.contains(match))
		{
			System.out.println("Matched with the provided text");
		}
		else
		{
			System.out.println("Not as same");
		}
		
	}

}
