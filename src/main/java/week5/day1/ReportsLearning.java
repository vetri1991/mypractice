package week5.day1;

import java.io.IOException;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class ReportsLearning {

	public static void main(String[] args) throws IOException {
		
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/result.html");
		html.setAppendExisting(true);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);
		
		//testcase level
		
		ExtentTest test = extent.createTest("TC003_Duplicate_Lead","Duplicating the Lead in LeafTaps");
		test.assignCategory("Smoke");
		test.assignAuthor("Vetri");
		//test step level
		
		test.pass("Browser Launched");
		test.pass("Lead Duplicated");
		
		test.pass("Browser Launched", MediaEntityBuilder.createScreenCaptureFromPath("./../snaps/img1.png").build());
		
		extent.flush();

	}

}
