package week5.day2;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.Date;
import java.util.HashSet;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import net.bytebuddy.agent.builder.AgentBuilder.CircularityLock;
import wdMethods.SeMethods;

public class ZoomCar extends ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "LeafTapsLearnings";
		testCaseDesc = "Create New Lead";
		category = "smoke";
		author ="Vetri";
	}
	@Test
	public void booking() throws InterruptedException
	{
		int arr[];
		//startApp("chrome", "https://www.zoomcar.com/chennai");
		WebElement startClick = locateElement("class", "search");
		click(startClick);
		
		WebElement pickLocation = locateElement("xpath", "//div[contains(text(),'Kodambakkam')]");
		click(pickLocation);
		
		WebElement nextButton = locateElement("class", "proceed");
		click(nextButton);
		
		Date date = new Date();
		DateFormat sdf = new SimpleDateFormat("dd");
		String todayDate = sdf.format(date);
		int tomorrow = Integer.parseInt(todayDate)+1;
		String tom = tomorrow+"";
		WebElement selection = locateElement("xpath", "//div[contains(text(),"+tomorrow+")]");
		click(selection);
		Thread.sleep(3000);
		
		WebElement nextButtonClick = locateElement("class", "proceed");
		click(nextButtonClick);
		
		WebElement journeyDate = locateElement("xpath", "//div[@class='day picked ']");
		////div[contains(text(),"+tomorrow+")]/div
		String text = journeyDate.getText();
		//System.out.println("Journey Date : " +text);
		verifyPartialText(journeyDate, tom);
		
		WebElement doneButton = locateElement("class", "proceed");
		click(doneButton);
		
		
		List<WebElement> findElementByXPath = driver.findElementsByXPath("//div[@class='price']");
		
		
		int size = findElementByXPath.size();
		//System.out.println("Number of Cars Listed out : "+size);
		arr = new int[size];
		for(int i=0; i<size;i++)
		{
			
			WebElement webElement = driver.findElementsByXPath("//div[@class='price']").get(i);
			
			String text1 = webElement.getText();
			//String replace = text1.replace("", "");
			//int length = text1.length();
			String substring = text1.substring(2, 5);
			//int value = Integer.parseInt(text);
			//System.out.println("Values are : "+replace);
			//System.out.println("Length : "+length);
			//System.out.println("Extract : "+substring);
			int price = Integer.parseInt(substring);
			//System.out.println("Extract : "+price);
			
			arr[i] = price;
			
			
		}
		
		int max = arr[0];
	       
        // Traverse array elements from second and
        // compare every element with current max  
        for (int i = 1; i < size; i++) 
        {
        	
        	
            if (arr[i] > max)
                max = arr[i];
      
        
        }
        System.out.println("Max Value "+max);
        WebElement carBrand = locateElement("xpath", "//div[@class='price']/preceding::h3");
        WebElement book = locateElement("xpath", "//div[contains(text(),"+max+")]/following::button");
        String text2 = carBrand.getText();
        System.out.println("Brand : "+text2);
        click(book);
        Thread.sleep(5000);
    }
}

		
	
