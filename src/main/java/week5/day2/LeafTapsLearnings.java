package week5.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class LeafTapsLearnings extends ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "LeafTapsLearnings";
		testCaseDesc = "Create New Lead";
		category = "smoke";
		author ="Vetri";
	}
	
	@Test
	public void createLead() {
		
//			startApp("chrome", "http://leaftaps.com/opentaps/control/main");
//			WebElement un = locateElement("id", "username");
//			type(un, "DemoSalesManager");
//			WebElement pwd = locateElement("id", "password");
//			type(pwd, "crmsfa");
//			WebElement login = locateElement("class", "decorativeSubmit");
//			click(login);
//			WebElement crm = locateElement("linktext", "CRM/SFA");
//			click(crm);
			WebElement createLead = locateElement("xpath", "//a[text()='Create Lead']");
			click(createLead);
			
			WebElement companyName = locateElement("id", "createLeadForm_companyName");
			type(companyName, "Cognizant");
			
			WebElement firstName = locateElement("id", "createLeadForm_firstName");
			type(firstName, "Velavan");
			
			WebElement lastName = locateElement("id", "createLeadForm_lastName");
			type(lastName, "Vijayasekar");
			
			WebElement salutation = locateElement("id", "createLeadForm_personalTitle");
			type(salutation,"vvvvv");
			
			
			
			WebElement source = locateElement("id", "createLeadForm_dataSourceId");
			selectDropDownUsingIndex(source, 1);
			
			
			
			WebElement createLeadSubmit = locateElement("class", "smallSubmit");
			click(createLeadSubmit);
			
			closeBrowser();
			
	}
	
}
