package week6.day2;
//package wdMethods;

import java.io.IOException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

//import wdMethods.ProjectMethods;
//import wdMethods.SeMethods;

public class LeafTapsLearnings extends wdMethods_Original.ProjectMethods{
	
	@BeforeTest
	public void setData()
	{
		testCaseName = "LeafTapsLearnings";
		testCaseDesc = "Create New Lead";
		category = "smoke";
		author ="Vetri";
		excel ="dataset.xlsx";
	}
	
	@Test(dataProvider="fetchdata")
	public void createLead(String cmp,String fn,String ln) {
		
//			startApp("chrome", "http://leaftaps.com/opentaps/control/main");
//			WebElement un = locateElement("id", "username");
//			type(un, "DemoSalesManager");
//			WebElement pwd = locateElement("id", "password");
//			type(pwd, "crmsfa");
//			WebElement login = locateElement("class", "decorativeSubmit");
//			click(login);
//			WebElement crm = locateElement("linktext", "CRM/SFA");
//			click(crm);
			WebElement createLead = locateElement("xpath", "//a[text()='Create Lead']");
			click(createLead);
			
			WebElement companyName = locateElement("id", "createLeadForm_companyName");
			type(companyName, cmp);
			
			WebElement firstName = locateElement("id", "createLeadForm_firstName");
			type(firstName, fn);
			
			WebElement lastName = locateElement("id", "createLeadForm_lastName");
			type(lastName, ln);
			
			WebElement salutation = locateElement("id", "createLeadForm_personalTitle");
			type(salutation,"vvvvv");
			
			
			
			WebElement source = locateElement("id", "createLeadForm_dataSourceId");
			selectDropDownUsingIndex(source, 1);
			
			
			
			WebElement createLeadSubmit = locateElement("class", "smallSubmit");
			click(createLeadSubmit);
			
			closeBrowser();
			
	}
	/*@DataProvider(name="fetchdata")
	public Object[][] fetchData() throws IOException{
		
		return ExcelPractice.getExcelWorks();
		
		Object[][] data = new Object[2][3];
		data[0][0] = "Cognizant";
		data[0][1] = "Velavan";
		data[0][2] = "M";
		
		data[1][0] = "ZOHO";
		data[1][1] = "Karthi";
		data[1][2] = "M";
		return data;	
	}*/
	
	
	
}
