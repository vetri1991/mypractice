package week6.day2;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelPractice {

	public static Object[][] getExcelWorks(String excel) throws IOException {
		
		XSSFWorkbook wbook = new XSSFWorkbook("./data/"+excel+".xlsx");
		//XSSFSheet sheet = wbook.getSheet("List");
		XSSFSheet sheet = wbook.getSheetAt(0);
		int rowCount = sheet.getLastRowNum();
		int columnCount = sheet.getRow(0).getLastCellNum();
		Object[][] data = new Object[rowCount][columnCount];
		for (int j = 1; j <= rowCount; j++) {
			XSSFRow row = sheet.getRow(j);
			for (int i = 0; i < columnCount; i++) {
				XSSFCell cell = row.getCell(i);
				data[j-1][i] = cell.getStringCellValue();
				//System.out.println("Column Values : " + stringCellValue);
			} 
		}

		wbook.close();
		return data;
		
	}

}
