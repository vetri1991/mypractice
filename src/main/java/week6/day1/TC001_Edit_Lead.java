package week6.day1;


import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;
import wdMethods.SeMethods;

public class TC001_Edit_Lead extends ProjectMethods{

	@BeforeTest
	public void setData()
	{
		testCaseName = "TC001_Edit_Lead";
		testCaseDesc = "Edit Lead";
		category = "smoke";
		author ="Vetri";
	}
	
	@Test(groups= {"sanity"})
	public void editLead() throws InterruptedException
	{
//		startApp("chrome", "http://leaftaps.com/opentaps/control/main");
//		WebElement un = locateElement("id", "username");
//		type(un, "DemoSalesManager");
//		WebElement pwd = locateElement("id", "password");
//		type(pwd, "crmsfa");
//		WebElement login = locateElement("class", "decorativeSubmit");
//		click(login);
//		WebElement crm = locateElement("linktext", "CRM/SFA");
//		click(crm);
		
		WebElement leads = locateElement("xpath", "//a[text()='Leads']");
		click(leads);
		
		WebElement findLeads = locateElement("xpath", "//a[text()='Find Leads']");
		click(findLeads);
		
		WebElement firstName = locateElement("xpath", "(//div[@class='x-form-element']/input[@name='firstName'])[3]");
		type(firstName, "velavan");
		
		WebElement findLeadsClickButton = locateElement("xpath", "//button[text()='Find Leads']");
		click(findLeadsClickButton);
		
		WebElement idClick = locateElement("xpath", "//table[@class='x-grid3-row-table']//a");
		click(idClick);
		
		WebElement title = locateElement("id", "sectionHeaderTitle_leads");
		String titleText = title.getText();
		verifyTitle(titleText);
		
		WebElement edit = locateElement("xpath", "//a[text()='Edit']");
		click(edit);
		
		WebElement companyName = locateElement("id", "updateLeadForm_companyName");
		companyName.clear();
		type(companyName, "CTS");
		
		WebElement updateButton = locateElement("xpath", "//input[@name='submitButton']");
		click(updateButton);
		
		WebElement companyNameVerify = locateElement("id", "viewLead_companyName_sp");
		String companyNameText = companyNameVerify.getText();
		verifyExactText(companyNameVerify, companyNameText);
		Thread.sleep(5000);
		closeBrowser();
		
		
	}
	
}
