package week3.day2;

//import java.util.List;


import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWenTable2 {

	public static void main(String[] args) {


		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://www.leafground.com/pages/table.html");
		driver.manage().window().maximize();
		//List<WebElement> allCheckbox = driver.findElementsByXPath("//input [@type='checkbox']");
		
		WebElement progress = driver.findElementByXPath("//font [contains(text(),'80')]");
		WebElement nextCheckBox = driver.findElementByXPath("//font [contains(text(),'80')]/following::input");
		
		String text = progress.getText();
		if(text.equals("80%"))
		{
			nextCheckBox.click();
		}
		//System.out.println(text);
		

	}

}
