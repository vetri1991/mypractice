package week3.day2;

import java.util.List;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class DropDown {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		
		driver.get("http://www.leafground.com/pages/Dropdown.html");
		WebElement src1 = driver.findElementByName("dropdown2");
		Select drop1 = new Select(src1);
		List<WebElement> options = drop1.getOptions();
		int size = options.size();
		drop1.selectByIndex(size-2);
		
		

	}

}
