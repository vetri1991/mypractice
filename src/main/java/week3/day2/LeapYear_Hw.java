package week3.day2;

import java.util.Scanner;

public class LeapYear_Hw {

	public static void main(String[] args) {
		
		int year,div=0;
		Scanner in = new Scanner(System.in);
		year = in.nextInt();
		div = year%4;
		if((div == 0)&&(year%400)==0)
		{
			System.out.println("Provided year is a leap year");
		}
		else
		{
			System.out.println("Its not a leap year");
		}
		
		in.close();
	}

}
