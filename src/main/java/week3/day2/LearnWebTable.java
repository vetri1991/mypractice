package week3.day2;

import java.util.List;

//import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class LearnWebTable {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		
		driver.get("http://www.leafground.com/pages/table.html");
		driver.manage().window().maximize();
		List<WebElement> allCheckbox = driver.findElementsByXPath("//input [@type='checkbox']");
		allCheckbox.get(2).click();
		int size = allCheckbox.size();
		
		System.out.println(size);

	}

}
