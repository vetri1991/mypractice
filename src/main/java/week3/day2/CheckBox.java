package week3.day2;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;

public class CheckBox {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/ChromeDriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().window().maximize();
		driver.get("http://www.leafground.com/pages/checkbox.html");
		WebElement scr = driver.findElementByXPath("//div[@class='example']//input");
		boolean enabled = scr.isSelected();
		if(enabled)
		{
			System.out.println(enabled);
		}
		else 
		{
			System.out.println("Not enabled");
		}

	}

}
