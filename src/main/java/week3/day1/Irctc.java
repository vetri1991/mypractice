package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

public class Irctc {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		driver.get("https://www.irctc.co.in/eticketing/userSignUp.jsf");
		driver.manage().window().maximize();
		driver.findElementById("userRegistrationForm:userName").sendKeys("Vetri1991");
		driver.findElementById("userRegistrationForm:password").sendKeys("28514662");
		driver.findElementById("userRegistrationForm:confpasword").sendKeys("28514662");
		WebElement src1 = driver.findElementById("userRegistrationForm:securityQ");
		Select drop1 = new Select(src1);
		List<WebElement> options = drop1.getOptions();
		int size = options.size();
		drop1.selectByIndex(size-7);
		driver.findElementById("userRegistrationForm:securityAnswer").sendKeys("GHMHSS");
		WebElement src2 = driver.findElementById("userRegistrationForm:prelan");
		Select drop2 = new Select(src2);
		//List<WebElement> options2 = drop2.getOptions();
		drop2.selectByIndex(0);
		driver.findElementById("userRegistrationForm:firstName").sendKeys("Velavan");
		WebElement src3 = driver.findElementById("userRegistrationForm:dobDay");
		Select drop3 = new Select(src3);
		drop3.selectByVisibleText("05");
		WebElement src4 = driver.findElementById("userRegistrationForm:dobMonth");
		Select drop4 = new Select(src4);
		drop4.selectByVisibleText("NOV");
		WebElement scr5 = driver.findElementById("userRegistrationForm:dateOfBirth");
		Select drop5 = new Select(scr5);
		drop5.selectByVisibleText("1991");
		WebElement scr6 = driver.findElementById("userRegistrationForm:occupation");
		Select drop6 = new Select(scr6);
		drop6.selectByVisibleText("Private");
		WebElement scr7 = driver.findElementById("userRegistrationForm:countries");
		Select drop7 = new Select(scr7);
		drop7.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:email").sendKeys("vetri.velan1991@gmail.com");
		driver.findElementById("userRegistrationForm:mobile").sendKeys("9600117381");
		WebElement scr8 = driver.findElementById("userRegistrationForm:nationalityId");
		Select drop8 = new Select(scr8);
		drop8.selectByVisibleText("India");
		driver.findElementById("userRegistrationForm:address").sendKeys("23");
		driver.findElementById("userRegistrationForm:pincode").sendKeys("603202",Keys.TAB);
		//driver.wait();
		//driver.findElementById("userRegistrationForm:statesName").sendKeys("",Keys.TAB);
		Thread.sleep(5000);
		WebElement scr9 = driver.findElementById("userRegistrationForm:cityName");
		Select drop9 = new Select(scr9);
		//List<WebElement> options2 = drop9.getOptions();
		//int size2 = options2.size();
		drop9.selectByVisibleText("Kanchipuram");
		Thread.sleep(5000);
		//drop9.selectByIndex(size2);
		WebElement scr10 = driver.findElementById("userRegistrationForm:postofficeName");
		Select drop10 = new Select(scr10);
		List<WebElement> options3 = drop10.getOptions();
		int size3 = options3.size();
		drop10.selectByIndex(size3-5);
		driver.findElementById("userRegistrationForm:landline").sendKeys("04428514662");
		
		Thread.sleep(10000);
		driver.close();
		

	}

}
