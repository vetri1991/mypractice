package week3.day1;

import java.util.List;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.support.ui.Select;

//import net.bytebuddy.implementation.bytecode.Throw;

public class Login {

	public static void main(String[] args) {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		try {
			
		
		driver.manage().window().maximize();
		
		driver.get("http://leaftaps.com/opentaps/");
		
		//Entering UserName
		driver.findElementById("username").sendKeys("DemoSalesManager");
		
		//Entering Password
		driver.findElementById("password").sendKeys("crmsfa");
		
		//Login
		driver.findElementByClassName("decorativeSubmit").click();
		
		//Clicking CRM/SFA
		driver.findElementByLinkText("CRM/SFA").click();
		
		//Clicking Create Lead
		driver.findElementByLinkText("Create Lead").click();
		
		//Entering Company name
		driver.findElementById("createLeadForm_companyName").sendKeys("Cognizant");
		
		//Entering First name
		driver.findElementById("createLeadForm_firstName").sendKeys("Velavan");
		
		//Entering Last name
		driver.findElementById("createLeadForm_lastName").sendKeys("Vijayasekar");
		
		//Storing element in a variable
		WebElement src = driver.findElementById("createLeadForm_dataSourceId");
		
		//Passing value to Select Class
		Select drop = new Select(src);
		
		//Selecting by Visible text
		drop.selectByVisibleText("Direct Mail");
		
		//Storing element in a variable
		WebElement src2 = driver.findElementById("createLeadForm_marketingCampaignId");
		
		//Passing value to Select Class
		Select drop2 = new Select(src2);
		
		//Storing all dropdown items in list
		List<WebElement> options = drop2.getOptions();
		int size = options.size();
		int index = size-2;	
		
		//Selecting by index
		drop2.selectByIndex(index);
		
		//Clicking Submit
		driver.findElementByClassName("smallSubmit").click();
		}
		catch (NoSuchElementException e) {
			//e.printStackTrace();
			System.out.println("Exception");
			//throw e;
		}
		finally {
			driver.close();
		}

	}

}
