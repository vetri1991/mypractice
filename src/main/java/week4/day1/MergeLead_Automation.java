package week4.day1;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.openqa.selenium.chrome.ChromeDriver;

public class MergeLead_Automation {

	public static void main(String[] args) throws InterruptedException {
		
		System.setProperty("webdriver.chrome.driver","./drivers/chromedriver.exe");
		ChromeDriver driver = new ChromeDriver();
		driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
		try {
			
			driver.get("http://leaftaps.com/opentaps/");
			driver.manage().window().maximize();
			driver.findElementById("username").sendKeys("DemoSalesManager");
			driver.findElementById("password").sendKeys("crmsfa");
			driver.findElementByClassName("decorativeSubmit").click();
			driver.findElementByLinkText("CRM/SFA").click();
			driver.findElementByLinkText("Leads").click();
			driver.findElementByLinkText("Merge Leads").click();
			driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdFrom']/following::a/img").click();
			
			Set<String> windowHandles = driver.getWindowHandles();
			List<String> list = new ArrayList<String>();
			list.addAll(windowHandles);
			
			driver.switchTo().window(list.get(1));
			
			driver.findElementByXPath("//div[@class='x-form-element']//input").sendKeys("10115");
			driver.findElementByXPath("//button[@class='x-btn-text']").click();
			Thread.sleep(2000);
			driver.findElementByXPath("//a[@class='linktext']").click();
			
			
			driver.switchTo().window(list.get(0));
			
			driver.findElementByXPath("//table[@id='widget_ComboBox_partyIdTo']/following-sibling::a").click();
			windowHandles = driver.getWindowHandles();
			list = new ArrayList<String>();
			list.addAll(windowHandles);
			int size = list.size();
			System.out.println(size);
			driver.switchTo().window(list.get(1));
			
			driver.findElementByXPath("//div[@class='x-form-element']//input").sendKeys("10116");
			driver.findElementByXPath("//button[@class='x-btn-text']").click();
			Thread.sleep(2000);
			driver.findElementByXPath("//a[@class='linktext']").click();
			
			driver.switchTo().window(list.get(0));
			
			driver.findElementByClassName("buttonDangerous").click();
			driver.switchTo().alert().accept();
			driver.findElementByXPath("//a[text()='Find Leads']").click();
			driver.findElementByXPath("(//div[@class='x-form-element'])[18]/input").sendKeys("10115");
			driver.findElementByXPath("(//button[@class='x-btn-text'])[7]").click();
			System.out.println(driver.findElementByXPath("//div[@class='x-paging-info']"));
			
			}
		
		catch(NoSuchElementException e)
		{
			System.out.println(e);
		}
		
		driver.close();
		
		
		

	}

}
