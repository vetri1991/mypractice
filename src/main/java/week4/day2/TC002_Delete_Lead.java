package week4.day2;

import java.util.NoSuchElementException;

import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;



import wdMethods.SeMethods;

public class TC002_Delete_Lead extends SeMethods {
	@BeforeTest
	public void setData()
	{
		testCaseName = "TC002_Delete_Lead";
		testCaseDesc = "Edit Lead";
		category = "smoke";
		author ="Vetri";
	}
	
	@Test(dependsOnMethods={"week4.day2.LeafTapsLearnings.createLead"})
	public void deleteLead() throws InterruptedException
	{
		try
		{
			
		
//	startApp("chrome", "http://leaftaps.com/opentaps/control/main");
//	WebElement un = locateElement("id", "username");
//	type(un, "DemoSalesManager");
//	WebElement pwd = locateElement("id", "password");
//	type(pwd, "crmsfa");
//	WebElement login = locateElement("class", "decorativeSubmit");
//	click(login);
//	WebElement crm = locateElement("linktext", "CRM/SFA");
//	click(crm);
	
	WebElement leads = locateElement("xpath", "//a[text()='Leads']");
	click(leads);
	WebElement findLeads = locateElement("xpath", "//a[text()='Find Leads']");
	click(findLeads);
	
	WebElement phone = locateElement("xpath", "//span[text()='Phone']");
	click(phone);
	
	WebElement areaCode = locateElement("xpath", "//input[@name='phoneAreaCode']");
	type(areaCode, "91");
	
	WebElement phoneNumber = locateElement("xpath", "//input[@name='phoneNumber']");
	type(phoneNumber, "9600117381");
	
	WebElement findLeadButton = locateElement("xpath", "//button[text()='Find Leads']");
	click(findLeadButton);
	
	WebElement leadIdClick = locateElement("xpath", "//table[@class='x-grid3-row-table']//a");
	String leadIdClicktext = leadIdClick.getText();
	click(leadIdClick);
	
	WebElement delete = locateElement("class", "subMenuButtonDangerous");
	click(delete);
	Thread.sleep(5000);
	WebElement findLeadsClick = locateElement("xpath", "//a[text()='Find Leads']");
	click(findLeadsClick);
	
	WebElement leadIdtype = locateElement("xpath", "//label[text()='Lead ID:']/following::input");
	type(leadIdtype, leadIdClicktext);
	
	WebElement findLeadButtonClick = locateElement("xpath", "//button[text()='Find Leads']");
	click(findLeadButtonClick);
	
	WebElement errormsg = locateElement("class", "x-paging-info");
	String errmsg = errormsg.getText();
	System.out.println(errmsg);
		}
		catch (NoSuchElementException e) {
			System.out.println("No Such Element Exception occured");
		}
	closeBrowser();
	}
	
}
