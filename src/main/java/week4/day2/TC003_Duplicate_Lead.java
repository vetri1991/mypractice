package week4.day2;

import org.openqa.selenium.WebElement;
import org.testng.annotations.Test;

import wdMethods.SeMethods;

public class TC003_Duplicate_Lead extends SeMethods {

	@Test
	public void duplicateLead()
	{
	startApp("chrome", "http://leaftaps.com/opentaps/control/main");
	
	WebElement un = locateElement("id", "username");
	type(un, "DemoSalesManager");
	WebElement pwd = locateElement("id", "password");
	type(pwd, "crmsfa");
	WebElement login = locateElement("class", "decorativeSubmit");
	click(login);
	WebElement crm = locateElement("linktext", "CRM/SFA");
	click(crm);
	
	WebElement leads = locateElement("xpath", "//a[text()='Leads']");
	click(leads);
	WebElement findLeads = locateElement("xpath", "//a[text()='Find Leads']");
	click(findLeads);
	
	WebElement email = locateElement("xpath", "//span[text()='Email']");
	click(email);
	
	WebElement emailAddress = locateElement("xpath", "//input[@name='emailAddress']");
	
	type(emailAddress, "dharsini011@gmail.com");
	
	WebElement findLeadButton = locateElement("xpath", "//button[text()='Find Leads']");
	click(findLeadButton);
	
	WebElement leadId = locateElement("xpath", "//table[@class='x-grid3-row-table']//a");
	String leadIdText = leadId.getText();
	click(leadId);
	
	WebElement duplicateLead = locateElement("xpath", "//a[text()='Duplicate Lead']");
	click(duplicateLead);
	
	WebElement verifyTitle = locateElement("id", "sectionHeaderTitle_leads");
	String text = verifyTitle.getText();
	verifyTitle(text);
	
	WebElement createLead = locateElement("class", "smallSubmit");
	click(createLead);
	
	WebElement idCompare = locateElement("id", "viewLead_companyName_sp");
	//String idText = idCompare.getText();
	
	verifyPartialText(idCompare, leadIdText);
	
	closeBrowser();
	
	
	}
}
