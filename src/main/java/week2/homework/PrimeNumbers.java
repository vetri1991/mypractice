package week2.homework;

import java.util.Scanner;

public class PrimeNumbers {

	public static void main(String[] args) {
		
			int n, count;
		        Scanner in = new Scanner(System.in);
		        System.out.println("Enter A Value : ");
		        n = in.nextInt();
		        System.out.println("Prime Numbers up to " + n);
		        for (int i = 2; i <= n; i++) {
		            count = 2;
		            for (int j = 2; j < i; j++) {
		                if (i % j == 0)
		                    count++;
		            }
		            if (count == 2) {
		                System.out.print("Prime number : "+i + " \n");
		                
		            }
		        }
		        in.close();
	}

}
