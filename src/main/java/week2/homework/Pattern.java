package week2.homework;

import java.util.Scanner;

public class Pattern {

	public static void main(String[] args) {

			Scanner S=new Scanner(System.in);
			System.out.println("Enter Inputs: ");
			int num1=S.nextInt();
			int num2=S.nextInt();
					
			for (int i=num1; i<=num2; i++)
			{
				if((i%3==0) && (i%5==0)) {
					System.out.print("FIZZBUZZ");
				}else if(i%5==0){
					System.out.print("BUZZ");
				}else if (i%3==0) {
					System.out.print("FIZZ");
				}else {
					System.out.print(i);
				}				
			System.out.print("\t");
			}
			S.close();
	}

}
