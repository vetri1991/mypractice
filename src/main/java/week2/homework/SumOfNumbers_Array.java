package week2.homework;

import java.util.Scanner;

public class SumOfNumbers_Array {

	public static void main(String[] args) {
		
		int input[],n,sum=0;
		Scanner in = new Scanner(System.in);
		System.out.println("Enter the total of numbers to be inserted : ");
		n = in.nextInt();
		input = new int[n];
		System.out.println("Enter the Values : ");
		for(int i=0; i<n; i++)
		{
			input[i] = in.nextInt();
			sum = sum +input[i];
		}
		System.out.println("Total Sum Value is : "+sum);
		in.close();
	}

}
